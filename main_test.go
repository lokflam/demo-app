package main

import (
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestHello(t *testing.T) {
	request, _ := http.NewRequest("GET", "/", nil)
	recorder := httptest.NewRecorder()
	GetRouter().ServeHTTP(recorder, request)

	assert.Equal(t, http.StatusOK, recorder.Code)
	assert.Equal(t, "Hello world!\n", recorder.Body.String())
}
