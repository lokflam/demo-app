FROM golang:1.16-alpine AS build

WORKDIR /build

RUN apk add build-base

COPY . .

RUN go mod download
RUN go build -o ./app

FROM alpine:3.14

WORKDIR /

COPY --from=build /build/app ./app

RUN addgroup --gid 1001 -S nonroot && \
  adduser -G nonroot --shell /bin/false --disabled-password -H --uid 1001 nonroot && \
  mkdir -p /var/log/nonroot && \
  chown nonroot:nonroot /var/log/nonroot && \
  chown nonroot:nonroot ./app

EXPOSE 8080

USER nonroot:nonroot

ENTRYPOINT ["./app"]
